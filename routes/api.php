<?php

use App\Http\Controllers\Api\V1\ProductsController as ProductsV1;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::apiResource('v1/products', ProductsV1::class)
      ->only(['index',]);

Route::get('update_products', [ProductsV1::class, 'update']);