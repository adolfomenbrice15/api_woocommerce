<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\V1\ProductsResource as ProductsResource;
use App\Models\Products;
use Illuminate\Http\Request;

use Automattic\WooCommerce\Client;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return ProductsResource::collection(Products::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update()
    {
        // Conexión WooCommerce API destino
        $url_API_woo = 'http://localhost';
        $ck_API_woo = 'ck_f60673d62719de270287305621be63321aad0cea';
        $cs_API_woo = 'cs_804ee80cad12fd01015f73687e5f69b6e88e0a98';

        $woocommerce = new Client(
            $url_API_woo,
            $ck_API_woo,
            $cs_API_woo,
            ['version' => 'wc/v3']
        );

        // Conexión API origen
        $url_API="http://localhost:8000/api/v1/products/";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$url_API);
        
        echo "➜ Obteniendo datos origen ... \n";
        $items_origin = curl_exec($ch);
        curl_close($ch);
        
        dd($items_origin);

        if ( ! $items_origin ) {
            exit('❗Error en API origen');
        }

        // Obtenemos datos de la API de origen
        $items_origin = json_decode($items_origin, true);

        $param_id ='';
        foreach ($items_origin as $item){
            $param_id .= $item['id'] . ',';
        }

        echo "➜ Obteniendo los ids de los productos... \n";
        // Obtenemos todos los productos de la lista de ids
        $products = $woocommerce->get('products/?id='. $param_id);
        
        // Construimos la data en base a los productos recuperados
        $item_data = [];
        foreach($products as $product){
        
            // Filtramos el array de origen por id
            $id = $product->id;
            $search_item = array_filter($items_origin, function($item) use($id) {
                return $item['id'] == $id;
            });
            $search_item = reset($search_item);
        
            // Formamos el array a actualizar
            $item_data[] = [
                'id' => $product->id,
                'regular_price' => $search_item['regular_price'],
                'stock_quantity' => $search_item['stock_quantity'],
            ];
        
        }
        
        // Construimos información a actualizar en lotes
        $data = [
            'update' => $item_data,
        ];
        
        echo "➜ Actualización en lote ... \n";
        // Actualización en lotes
        $result = $woocommerce->post('products/batch', $data);
        
        if (! $result) {
            echo("❗Error al actualizar productos \n");
        } else {
            print("✔ Productos actualizados correctamente \n");
        }
    }
}
